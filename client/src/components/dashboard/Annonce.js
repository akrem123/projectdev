import React, { Component } from "react";
import { connect } from "react-redux";
import propTypes from "prop-types";
import { deleteAnnonce } from "../../actions/profileActions";
import SingleAnnonce from "./SingleAnnonce";

class Annonce extends Component {
  onDeleteClick = id => {
    this.props.deleteAnnonce(id);
  };

  onEditClick = id => {
    this.props.editAnnonce(id);
  };

  render() {
    const annonce = this.props.annonce
      .sort(function(a, b) {
        return +new Date(a.from) - +new Date(b.from);
      })
      .reverse()
      .map(ann => (
        <SingleAnnonce
          ann={ann}
          key={ann._id}
          onDeleteClick={this.onDeleteClick}
        />
      ));

    return (
      <div>
        <h4 className="mb-5 center-mobile">Listes annonces</h4>
        <div className="evenly-space bold hide-when-tablet">
          <div className="space center">Titre</div>
          <div className="space center">Prix</div>
          <div className="space center">Adresse</div>
          <div className="space center">Type</div>
          <div className="space center">Status</div>
          <div className="space center">Surface</div>
          <div className="space center">Description</div>
        </div>
        {annonce}
      </div>
    );
  }
}

deleteAnnonce.propTypes = {
  deleteAnnonce: propTypes.func.isRequired
};

export default connect(
  null,
  { deleteAnnonce }
)(Annonce);
