import React, { Component , Fragment} from "react";

import styled from "styled-components";
import { connect } from "react-redux";

import TextFieldGroup from "../common/TextFieldGroup";

import { withRouter } from "react-router-dom";
import {
  updateAgence,
  getCurrentProfile
} from "../../actions/profileActions";
import propTypes from "prop-types";

const Form = styled.form`
  margin-bottom: 1rem;
  margin-top: 1rem;
  border-top: 1px solid #6c757d;
  padding-top: 1rem;
`;

const AgenceDetails = styled.div`
  margin-bottom: 1rem;
`;

class SingleAgence extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nom: "",
      adresse: "",
      region: "",
      
      errors: {},
      disabled: false,
      editMode: false,
      nomEdit: "",
      adresseEdit: "",
      regionEdit: "",
      
      id: 0
    };
  }

  componentDidMount() {
    this.setState({
      nom: this.props.age.nom,
      adresse: this.props.age.adresse,
      region: this.props.age.region,
      
      id: this.props.age._id,
      
    });
  }

  onDeleteClick = id => {
    this.props.onDeleteClick(id);
  };

  onEditClick = id => {
    this.setState({
      editMode: !this.state.editMode,
      nomEdit: this.state.nom,
      adresseEdit: this.state.adresse,
      regionEdit: this.state.region,
      
    });
  };
  onCancelClick = id => {
    this.setState({
      editMode: !this.state.editMode,
      nom: this.state.nomEdit,
      adresse: this.state.adresseEdit,
      region: this.state.regionEdit,
      
    });
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onCheck = e => {
    this.setState({
      current: !this.state.current
    });
  };

  onSubmit = e => {
    e.preventDefault();
  };

  onUpdate = id => {
    const { age, updateAgence } = this.props;

    this.setState({
      editMode: !this.state.editMode,
      nomEdit: this.state.nom,
      adresseEdit: this.state.adresse,
      regionEdit: this.state.region,
      
      errors: {}
    });

    const ageData = {
      nom: this.state.nom,
      adresse: this.state.adresse,
      region: this.state.region,
      
      id: id
    };

    updateAgence(ageData, age._id);
  };

  componentWillReceiveProps(nextProps) {
    if (typeof nextProps.errors[nextProps.age._id] !== "undefined") {
      if (Object.keys(nextProps.errors[nextProps.age._id]).length === 0) {
        this.setState({
          editMode: false
        });
      } else {
        this.setState({
          editMode: true
        });
        this.setState({
          errors: {
            nom: nextProps.errors[nextProps.age._id].nom,
            adresse: nextProps.errors[nextProps.age._id].adresse,
            region: nextProps.errors[nextProps.age._id].region,
            
            id: nextProps.errors[nextProps.age._id].id,
            
          }
        });
      }
    }
  }

  render() {
    const { age } = this.props;
    const { editMode, errors } = this.state;

    const actionButtons = (
      <div className="space center">
        {!editMode ? (
          <Fragment>
            <button
              className="btn btn-info"
              onClick={this.onEditClick.bind(this, age._id)}
            >
              Modifier
            </button>{" "}
            <button
              className="btn btn-danger"
              onClick={this.onDeleteClick.bind(this, age._id)}
            >
              Supprimer
            </button>
          </Fragment>
        ) : (
          <Fragment>
            <button
              className="btn btn-info"
              onClick={this.onUpdate.bind(this, age._id)}
            >
              Confirmer
            </button>
            <button
              className="btn btn-danger"
              onClick={this.onCancelClick.bind(this, age._id)}
            >
                Annuler
            </button>
          </Fragment>
        )}
      </div>
    );

    return (
      <Form onSubmit={this.onSubmit}>
        <AgenceDetails className="evenly-space column-tablet">
          <div className="space center">
            <div className="bold show-tablet">Nom</div>
            {!editMode ? (
              this.state.nom
            ) : (
              <TextFieldGroup
                placeholder="* Nom"
                name="nom"
                value={this.state.nom}
                onChange={this.onChange}
                error={errors.nom}
              />
            )}
          </div>
          <div className="space center">
            <div className="bold show-tablet">Adresse</div>
            {!editMode ? (
              this.state.adresse
            ) : (
              <TextFieldGroup
                placeholder="* Agence Adresse"
                name="adresse"
                value={this.state.adresse}
                onChange={this.onChange}
                error={errors.adresse}
              />
            )}
          </div>
          
                
            
          <div className="space center">
            <div className="bold show-tablet">Region</div>
            {!editMode ? (
              this.state.region
            ) : (
              <TextFieldGroup
                placeholder="Location"
                name="region"
                value={this.state.region}
                onChange={this.onChange}
                error={errors.region}
              />
            )}
          </div>
          
          
        </AgenceDetails>
        {actionButtons}
      </Form>
    );
  }
}

SingleAgence.proptypes = {
  updateAgence: propTypes.func.isRequired,
  agence: propTypes.object.isRequired,
  errors: propTypes.object.isRequired
};

const mapStateToProps = state => ({
  agence: state.edu,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { updateAgence, getCurrentProfile }
)(withRouter(SingleAgence));
