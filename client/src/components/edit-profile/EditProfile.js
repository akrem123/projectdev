import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import TextFieldGroup from '../common/TextFieldGroup';
import TextAreaFieldGroup from '../common/TextAreaFieldGroup';
import InputGroup from '../common/InputGroup';
import { createProfile, getCurrentProfile } from '../../actions/profileActions';
import isEmpty from '../../validation/is-empty';
class CreateProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displaySocialInputs: false,
      handle: '',
      poste: '',
      tel: '',
      emailadr: '',
      bio: '',
     
      facebook: '',
      linkedin: '',
      instagram: '',
      errors: {}
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    this.props.getCurrentProfile();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }

    if (nextProps.profile.profile) {
      const profile = nextProps.profile.profile;

      

      // If profile field doesnt exist, make empty string
      
      profile.emailadr = !isEmpty(profile.emailadr) ? profile.emailadr : '';
      profile.tel = !isEmpty(profile.tel) ? profile.tel : '';
      
      profile.bio = !isEmpty(profile.bio) ? profile.bio : '';
      profile.social = !isEmpty(profile.social) ? profile.social : {};
      
      profile.facebook = !isEmpty(profile.social.facebook)
        ? profile.social.facebook
        : '';
      profile.linkedin = !isEmpty(profile.social.linkedin)
        ? profile.social.linkedin
        : '';
      
      profile.instagram = !isEmpty(profile.social.instagram)
        ? profile.social.instagram
        : '';

      // Set component fields state
      this.setState({
        handle: profile.handle,
        poste: profile.poste,
        tel: profile.tel,
        emailadr: profile.emailadr,
      
        bio: profile.bio,
        
        facebook: profile.facebook,
        linkedin: profile.linkedin,
        
        instagram: profile.instagram
      });
    }
  }

  onSubmit(e) {
    e.preventDefault();

    const profileData = {
      handle: this.state.handle,
      poste: this.state.poste,
      tel: this.state.tel,
      emailadr: this.state.emailadr,
      
      bio: this.state.bio,
      
      facebook: this.state.facebook,
      linkedin: this.state.linkedin,
     
      instagram: this.state.instagram
    };

    this.props.createProfile(profileData, this.props.history);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    const { errors, displaySocialInputs } = this.state;

    let socialInputs;

    if (displaySocialInputs) {
      socialInputs = (
        <div>
          

          <InputGroup
            placeholder="Facebook Page URL"
            name="facebook"
            icon="fab fa-facebook"
            value={this.state.facebook}
            onChange={this.onChange}
            error={errors.facebook}
          />

          <InputGroup
            placeholder="Linkedin Profile URL"
            name="linkedin"
            icon="fab fa-linkedin"
            value={this.state.linkedin}
            onChange={this.onChange}
            error={errors.linkedin}
          />

          

          <InputGroup
            placeholder="Instagram Page URL"
            name="instagram"
            icon="fab fa-instagram"
            value={this.state.instagram}
            onChange={this.onChange}
            error={errors.instagram}
          />
        </div>
      );
    }

    

    return (
      <div className="create-profile">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <Link to="/dashboard" className="btn btn-light">
                Retour
              </Link>
              <h1 className="display-4 text-center">Modifier Profile</h1>
              <small className="d-block pb-3">* = Champs obligatoire</small>
              <form onSubmit={this.onSubmit}>
                <TextFieldGroup
                  placeholder="* Nom"
                  name="handle"
                  value={this.state.handle}
                  onChange={this.onChange}
                  error={errors.handle}
                  info="Une poignée unique pour l'URL de votre profil. Votre nom complet, nom de l'entreprise, surnom"
                />
                
                <TextFieldGroup
                  placeholder="* Poste"
                  name="poste"
                  value={this.state.poste}
                  onChange={this.onChange}
                  error={errors.poste}
                  info=" votre propre poste "
                />
                <TextFieldGroup
                  placeholder="Telephone"
                  name="tel"
                  value={this.state.tel}
                  onChange={this.onChange}
                  error={errors.tel}
                  info="Votre propre téléphone "
                />
                <TextFieldGroup
                  placeholder="* Email"
                  name="emailadr"
                  value={this.state.emailadr}
                  onChange={this.onChange}
                  error={errors.emailadr}
                  info="Email de l'agent"
                />
                
                
                <TextAreaFieldGroup
                  placeholder="Petit bio"
                  name="bio"
                  value={this.state.bio}
                  onChange={this.onChange}
                  error={errors.bio}
                  info="Parle-nous un peu de toi "
                />

                <div className="mb-3">
                  <button
                    type="button"
                    onClick={() => {
                      this.setState(prevState => ({
                        displaySocialInputs: !prevState.displaySocialInputs
                      }));
                    }}
                    className="btn btn-light"
                  >
                    Ajouter contact réseaux sociaux
                  </button>
                  <span className="text-muted">Optionel</span>
                </div>
                {socialInputs}
                <input
                  type="submit"
                  value="Confirmer"
                  className="btn btn-info btn-block mt-4"
                />

              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

CreateProfile.propTypes = {
  createProfile: PropTypes.func.isRequired,
  getCurrentProfile: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  profile: state.profile,
  errors: state.errors
});

export default connect(mapStateToProps, { createProfile, getCurrentProfile })(
  withRouter(CreateProfile)
);
