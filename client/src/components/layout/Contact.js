import React, { Component } from 'react'

import { FaFacebook,FaGooglePlus } from "react-icons/fa"
import './Contact.css'




export class Contact extends Component {
    render() {
        return (
            <div>
            
<br/> <br/>

<div className="b">



            <section className="maaxen-contact-page-area section_70">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8 col-sm-7">
                            <div className="contact-page-left">
                                <h3>Contacter-nous</h3>
                                <form>
                                    <p>
                                        <input type="text" placeholder="Votre Nom"/>
                                    </p>
                                    <p>
                                        <input type="email" placeholder="Adresse mail"/>
                                    </p>
                                    <p>
                                        <input type="text" placeholder="Objet"/>
                                    </p>
                                    <p>
                                        <textarea placeholder="Votre Question..."></textarea>
                                    </p>
                                    <p>
                                        <button type="submit">Envoyer Message</button>
                                    </p>
                                </form>
                            </div>
                        </div>
                        <div className="col-lg-4 col-sm-5"></div>
                        <div className="contact-page-right">
                            <div className="contact-page-widget">
                                <h4>
                                    "Abonnez-vous"
                                </h4>
                                <ul>
                                    <li>
                                       
                                           <FaFacebook />
                                        
                                    </li>
                                    <li>
                                            <FaGooglePlus />
                                    </li>
                                </ul>
                            </div>
                            <div className="contact-page-widget">
                            <h4>
                                    "Obtenir de l'aide"
                                </h4>
                                <ul className="contact-address">
                                    <li>                                 
                                         <span> Addresse :</span>
                                         "Sousse, Tunisia, 4000"
                                    
                                    </li>
                                    <li>
                                          <span>Email :</span>
                                          "contact@yoodev-it.tn"
                                    </li>
                                    <li>
                                          <span>Phone :</span>
                                          "+21629 972 490"
                                    </li>
                                    <li>
                                          <span>Web :</span>
                                          "http://www.yoodev-it.tn"
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </section>
            </div>
    
    </div>
        )
    }
}

export default Contact
