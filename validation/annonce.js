const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateAnnonceInput(data) {
  let errors = {};

  data.title = !isEmpty(data.title) ? data.title : '';
  data.prix = !isEmpty(data.prix) ? data.prix : '';
  data.status = !isEmpty(data.status) ? data.status : '';

  if (Validator.isEmpty(data.title)) {
    errors.title = 'Annonce title field is required';
  }

  if (Validator.isEmpty(data.prix)) {
    errors.prix = 'le champ prix est obligatoire';
  }

  if (Validator.isEmpty(data.status)) {
    errors.status = 'Le champ Annonce est obligatoire';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
