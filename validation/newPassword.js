const Validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = function validateNewPasswordInput(data) {
  let errors = {};

  data.email = !isEmpty(data.email) ? data.email : "";
  data.password = !isEmpty(data.password) ? data.password : "";

  if (!Validator.isEmail(data.email)) {
    errors.email = "Email est invalide";
  }

  if (Validator.isEmpty(data.email)) {
    errors.email = "Le champ e-mail est obligatoire";
  }

  if (!Validator.isLength(data.email, { min: 2, max: 30 })) {
    errors.email = "L'adresse e-mail saisie est incorrecte";
  }

  if (Validator.isEmpty(data.password)) {
    errors.password = "Le champ du mot de passe est obligatoire";
  }

  if (!Validator.isLength(data.password, { min: 2, max: 30 })) {
    errors.password = "Le mot de passe est incorrect";
  }

  data.newpassword = !isEmpty(data.newpassword) ? data.newpassword : "";
  data.confirmNewPassword = !isEmpty(data.confirmNewPassword)
    ? data.confirmNewPassword
    : "";

  if (!Validator.isLength(data.newpassword, { min: 6, max: 30 })) {
    errors.newpassword = "Le mot de passe doit comprendre entre 6 et 30 caractères";
  }

  if (Validator.isEmpty(data.newpassword)) {
    errors.newpassword = "Le champ du mot de passe est obligatoire";
  }

  if (Validator.isEmpty(data.confirmNewPassword)) {
    errors.confirmNewPassword = "Le champ Confirmer le mot de passe est obligatoire";
  } else {
    if (!Validator.equals(data.newpassword, data.confirmNewPassword)) {
      errors.confirmNewPassword = "les mots de passe doivent correspondre";
    }
  }
  if (!Validator.isLength(data.confirmNewPassword, { min: 6, max: 30 })) {
    errors.confirmNewPassword =
      "Le champ Confirmer le mot de passe doit contenir entre 6 et 30 caractères";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
