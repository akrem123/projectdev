const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');

// Load Validation
const validateProfileInput = require('../../validation/profile');
const validateAnnonceInput = require('../../validation/annonce');
const validateAgenceInput = require('../../validation/agence');

// Load Profile Model
const Profile = require('../../models/Profile');
// Load User Model
const User = require('../../models/User');

// @route   GET api/profile/test
// @desc    Tests profile route
// @access  Public
router.get('/test', (req, res) => res.json({ msg: 'Profile Works' }));

// @route   GET api/profile
// @desc    Get current users profile
// @access  Private
router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const errors = {};

    Profile.findOne({ user: req.user.id })
      .populate('user', ['name', 'avatar'])
      .then(profile => {
        if (!profile) {
          errors.noprofile = 'There is no profile for this user';
          return res.status(404).json(errors);
        }
        res.json(profile);
      })
      .catch(err => res.status(404).json(err));
  }
);

// @route   GET api/profile/all
// @desc    Get all profiles
// @access  Public
router.get('/all', (req, res) => {
  const errors = {};

  Profile.find()
    .populate('user', ['name', 'avatar'])
    .then(profiles => {
      if (!profiles) {
        errors.noprofile = 'There are no profiles';
        return res.status(404).json(errors);
      }

      res.json(profiles);
    })
    .catch(err => res.status(404).json({ profile: 'There are no profiles' }));
});

// @route   GET api/profile/handle/:handle
// @desc    Get profile by handle
// @access  Public

router.get('/handle/:handle', (req, res) => {
  const errors = {};

  Profile.findOne({ handle: req.params.handle })
    .populate('user', ['name', 'avatar'])
    .then(profile => {
      if (!profile) {
        errors.noprofile = 'There is no profile for this user';
        res.status(404).json(errors);
      }

      res.json(profile);
    })
    .catch(err => res.status(404).json(err));
});


// @route   GET api/profile/user/:user_id
// @desc    Get profile by user ID
// @access  Public

router.get('/user/:user_id', (req, res) => {
  const errors = {};

  Profile.findOne({ user: req.params.user_id })
    .populate('user', ['name', 'avatar'])
    .then(profile => {
      if (!profile) {
        errors.noprofile = 'There is no profile for this user';
        res.status(404).json(errors);
      }

      res.json(profile);
    })
    .catch(err =>
      res.status(404).json({ profile: 'There is no profile for this user' })
    );
});

// @route   POST api/profile
// @desc    Create or Edit user profile
// @access  Private
router.post(
  '/', 
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { errors, isValid } = validateProfileInput(req.body);
    // Check Validation
    if (!isValid) {
      // Return any errors with 400 status
      return res.status(400).json(errors);
    }

    // Get fields
    const profileFields = {};
    profileFields.user = req.user.id;
    if (req.body.handle) profileFields.handle = req.body.handle;
    if (req.body.poste) profileFields.poste = req.body.poste;
    if (req.body.tel) profileFields.tel = req.body.tel;
    if (req.body.emailadr) profileFields.emailadr = req.body.emailadr;
    if (req.body.bio) profileFields.bio = req.body.bio;
    
    
    // Social (optional fields)
    profileFields.social = {};
  
    if (req.body.facebook) profileFields.social.facebook = req.body.facebook;
    if (req.body.linkedin) profileFields.social.linkedin = req.body.linkedin;
    if (req.body.instagram) profileFields.social.instagram = req.body.instagram;

    // Create or Edit current user profile with unique handle
    Profile
      .findOne({ user: req.user.id })
      .then(profile => {
        // If profile not exist, then create a new one, Otherwise just update 
        
        // Create new profile
        if(!profile){
          // Check if handle exists (handle should be unoque for all profile)
          Profile
            .findOne({ handle: profileFields.handle})
            .then(profile => {
            if(profile){
              errors.handle = 'handle already exists';
              res.status(400).json(errors);
            }
          });
          new Profile(profileFields).save().then(profile => res.json(profile));
        }
        // Update the profile
        else{
          // Check if handle exists for other user
          Profile
            .findOne({ handle: profileFields.handle})
            .then(p => {
            if(profile.handle !== p.handle){
              errors.handle = 'handle already exists';
              res.status(400).json(errors);
            }
          });
          Profile
            .findOneAndUpdate(
              {user: req.user.id},
              {$set: profileFields},
              {new: true}
            )
            .then(profile => res.json(profile));
        }
      });
  }
);






// @route   POST api/profile/annonce
// @desc    Add annonce to profile
// @access  Private
router.post(
  '/annonce',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { errors, isValid } = validateAnnonceInput(req.body);

    // Check Validation
    if (!isValid) {
      // Return any errors with 400 status
      return res.status(400).json(errors);
    }

    Profile.findOne({ user: req.user.id }).then(profile => {
      const newAnn = {
        title: req.body.title,
        prix: req.body.prix,
        adressebien: req.body.adressebien,
        typeannonce: req.body.typeannonce,
        status: req.body.status,
        surface: req.body.surface,
        description: req.body.description,
        //add image
        productPic: req.body.productPic
      };

      // Add to ann array
      profile.annonce.unshift(newAnn);

      profile.save().then(profile => res.json(profile));
    });
  }
);

// @route   POST api/profile/agence
// @desc    Add agence to profile
// @access  Private
router.post(
  '/agence',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { errors, isValid } = validateAgenceInput(req.body);

    // Check Validation
    if (!isValid) {
      // Return any errors with 400 status
      return res.status(400).json(errors);
    }

    Profile.findOne({ user: req.user.id }).then(profile => {
      const newAge = {
        nom: req.body.nom,
        adresse: req.body.adresse,
        region: req.body.region,
        
      };

      // Add to agence array
      profile.agence.unshift(newAge);

      profile.save().then(profile => res.json(profile));
    });
  }
);

// @route   DELETE api/profile/annonce/:ann_id
// @desc    Delete annonce from profile
// @access  Private
router.delete(
  '/annonce/:ann_id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    Profile.findOne({ user: req.user.id })
      .then(profile => {
        // Get remove index
        const removeIndex = profile.annonce
          .map(item => item.id)
          .indexOf(req.params.ann_id);

        // Splice out of array
        profile.annonce.splice(removeIndex, 1);

        // Save
        profile.save().then(profile => res.json(profile));
      })
      .catch(err => res.status(404).json(err));
  }
);

// @route   DELETE api/profile/agence/:age_id
// @desc    Delete agence from profile
// @access  Private
router.delete(
  '/agence/:age_id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    Profile.findOne({ user: req.user.id })
      .then(profile => {
        // Get remove index
        const removeIndex = profile.agence
          .map(item => item.id)
          .indexOf(req.params.age_id);

        // Splice out of array
        profile.agence.splice(removeIndex, 1);

        // Save
        profile.save().then(profile => res.json(profile));
      })
      .catch(err => res.status(404).json(err));
  }
);

// @route   DELETE api/profile
// @desc    Delete user and profile
// @access  Private
router.delete(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    Profile.findOneAndRemove({ user: req.user.id }).then(() => {
      User.findOneAndRemove({ _id: req.user.id }).then(() =>
        res.json({ success: true })
      );
    });
  }
);
// @route   POST api/profile/agence/:id
// @desc    Update an agence
// @access  Private
router.post(
  "/agence/:age_id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { errors, isValid } = validateAgenceInput(req.body);

    if (!isValid) {
      // Return any errors with 400 status
      return res.status(400).json(errors);
    }

    Profile.findOne({ user: req.user.id })
      .then(profile => {
        const itemNumber = profile.agence
          .map(item => item.id)
          .indexOf(req.params.age_id);

        let itemToUpdate = profile.agence[itemNumber];

          (itemToUpdate.nom = req.body.nom),
          (itemToUpdate.adresse = req.body.adresse),
          (itemToUpdate.region = req.body.region),
          

        profile.save().then(profile => res.json(profile));
      })
      .catch(err => res.status(404).json(err));
  }
);
// @route   POST api/profile/annonce/:id
// @desc    Update an annonce
// @access  Private
router.post(
  "/annonce/:ann_id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { errors, isValid } = validateAnnonceInput(req.body);

    if (!isValid) {
      // Return any errors with 400 status
      return res.status(400).json(errors);
    }

    Profile.findOne({ user: req.user.id })
      .then(profile => {
        const itemNumber = profile.annonce
          .map(item => item.id)
          .indexOf(req.params.ann_id);

        let itemToUpdate = profile.annonce[itemNumber];

        itemToUpdate.title = req.body.title;
        itemToUpdate.prix = req.body.prix;
        itemToUpdate.adressebien = req.body.adressebien;
        itemToUpdate.typeannonce = req.body.typeannonce;
        itemToUpdate.status = req.body.status;
        itemToUpdate.description = req.body.description;
        itemToUpdate.surface = req.body.surface;

        profile.save().then(profile => res.json(profile));
      })
      .catch(err => res.status(404).json(err));
  }
);

module.exports = router;
